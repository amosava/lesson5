﻿namespace Lesson5_Strings
{
    public class Task1
    {
        public static void RunApp1()
        {
            Console.WriteLine("Please, enter a string: ");
            string? userInput = Console.ReadLine();

            string? replacedText = ReplaceTestText(userInput);
            string? removedDigitsFromString = RemoveDigits(replacedText);

            Console.WriteLine($"Initial string: {userInput}");
            Console.WriteLine($"New string: {removedDigitsFromString}");
        }

        static string ReplaceTestText(string userInput)
        {
            string replacedText = userInput.Replace("test", "testing");
            return replacedText;
        }

        static string RemoveDigits(string input)
        {
            char[] chars = input.ToCharArray();
            int count = 0;
            char[] newCharArray = new char[chars.Length];

            for (int i = 0; i < chars.Length; i++)
            {
                if (!char.IsDigit(chars[i]))
                {
                    newCharArray[count] = chars[i];
                    count++;
                }
            }

            return new string(newCharArray, 0, count);
        }
    }
}