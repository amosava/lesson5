﻿using System.Text;

namespace Lesson5_Strings
{
    public class Task2
    {
        public static void RunApp2()
        {
            string[] wordsTMS = { "Welcome", "to", "the", "TMS", "lessons." };

            StringBuilder concatResult = new StringBuilder();

            foreach (string word in wordsTMS)
            {
                concatResult.Append("\"");
                concatResult.Append(word);
                concatResult.Append("\" ");
            }

            Console.WriteLine(concatResult.ToString().Trim());
        }
    }
}
