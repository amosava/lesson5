﻿using System.Text.RegularExpressions;

namespace Lesson5_Strings
{
    internal class Task3
    {
        public static void RunApp3()
        {
            // teamwithsomeofexcersicesabcwanttomakeitbetter - for test
            Console.WriteLine("Please, enter a string: ");
            string? userInput = Console.ReadLine();

            string regPattern = "(.*?)(abc)(.*)";

            Match match = Regex.Match(userInput, regPattern);

            if (match.Success)
            {
                string beforeABC = match.Groups[1].Value;
                string afterABC = match.Groups[3].Value;

                Console.WriteLine(beforeABC + afterABC);
            }
            else
            {
                Console.WriteLine(userInput);
            }
        }
    }
}
