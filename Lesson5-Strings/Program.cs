﻿using Lesson5_Strings;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Choose the app:");
        Console.WriteLine("Task 1, Task 2, Task 3, Task 4, Task 5");

        string? userChoice = Console.ReadLine();
        userChoice?.Trim();
        userChoice?.ToLower();

        switch (userChoice)
        {
            case "task 1":
                Task1.RunApp1();
                break;
            case "task 2":
                Task2.RunApp2();
                break;
            case "task 3":
                Task3.RunApp3();
                break;
            case "task 4":
                Task4.RunApp4();
                break;
            case "task 5":
                 Task5.RunApp5();
                 break;
            default:
                Console.WriteLine("Incorrect choice");
                break;
        }
    }
}
