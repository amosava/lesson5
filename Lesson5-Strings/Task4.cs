﻿namespace Lesson5_Strings
{
    internal class Task4
    {
        public static void RunApp4()
        {
            string badString = "Bad day.";
            string goodString = "Good day!!!!!!!!!";

            int zeroIndex = badString.ToLower().IndexOf("bad");

            string beforeBadWord = badString[..zeroIndex];
            string stringAfterReplacement = beforeBadWord + goodString;
            int lastIndex = stringAfterReplacement.LastIndexOf("!");

            stringAfterReplacement = stringAfterReplacement.Remove(lastIndex, 1).Insert(lastIndex, "?");

            Console.WriteLine(stringAfterReplacement);
        }
    }
}
