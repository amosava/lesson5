﻿namespace Lesson5_Strings
{
    internal class Task5
    {
        public static void RunApp5()
        {
            Console.WriteLine("Please, choose the program: PrintFirstTwoBlocks, LettersReplace, CheckABC, StartsWith555, EndsWith1a2b");
            string? userChoice = Console.ReadLine();
            userChoice?.Trim();
            userChoice?.ToLower();

            Console.WriteLine("Enter the document number of the xxxx-yyy-xxxx-yyy-xyxy format: ");
            string? documentNumber = Console.ReadLine();
            documentNumber = documentNumber.ToLower();

            switch (userChoice)
            {
                case "PrintFirstTwoBlocks":
                    PrintFirstTwoBlocks(documentNumber);
                    break;
                case "LettersReplace":
                    LettersReplace(documentNumber);
                    break;
                case "CheckABC":
                    CheckABC(documentNumber);
                    break;
                case "StartsWith555":
                    StartsWith555(documentNumber);
                    break;
                case "EndsWith1a2b":
                    EndsWith1a2b(documentNumber);
                    break;
                default:
                    Console.WriteLine("Incorrect choice");
                    break;
            }
        }

        static void PrintFirstTwoBlocks(string? documentNumber)
        {
            if (documentNumber.Length >= 8)
            {
                string firstBlock = documentNumber.Substring(0, 4);
                string secondBlock = documentNumber.Substring(5, 3);
                Console.WriteLine($"First two blocks are: {firstBlock} and {secondBlock}");
            }
            else
            {
                Console.WriteLine("Error. Wrong document number.");
            }
        }

        static void LettersReplace(string? documentNumber)
        {
            if (documentNumber.Contains("abc"))
            {
                string abcNumber = documentNumber.Replace("abc", "***").Replace("ABC", "***");
                Console.WriteLine($"Doc number with replaced \"abc\": {abcNumber}");
            }
            else
            { Console.WriteLine("The doc number doesn't contain \"abc/ABC\""); }
        }

        static void CheckABC(string? documentNumber)
        {
            bool containsAbc = documentNumber.Contains("abc", StringComparison.OrdinalIgnoreCase);

            if (containsAbc) { Console.WriteLine($"The doc number contains abs/ABC."); }
            else { Console.WriteLine("The entered number doesn't contain \"abc/ABC\" strings"); }

        }

        static void StartsWith555(string? documentNumber)
        {
            bool startsWith555 = documentNumber.StartsWith("555");

            if (startsWith555) { Console.WriteLine($"The doc number starts with \"555\""); }
            else { Console.WriteLine($"The doc number doesn't start with \"555\""); }
        }

        static void EndsWith1a2b(string? documentNumber)
        {
            bool endsWith1a2b = documentNumber.EndsWith("1a2b");

            if (endsWith1a2b) { Console.WriteLine($"The doc number ends with \"1a2b\""); }
            else { Console.WriteLine($"The doc number doesn't end with \"1a2b\""); }
        }
    }
}
